
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "stdio.h"

int main(){
  int fd;
  char *nom="/tmp/FIFOtenis";
  char c;
  
  
  if (mkfifo(nom,0666)==0)
  {
  
  	fd =open (nom, O_RDONLY);	
  	if (fd<0){
  	perror("Error en open");
  	return 1;
  	}

  	
  	while (read(fd,&c,1)==1)
		write (1,&c,1);
	
	close(fd);
	unlink(nom);
	
  }
  else{
  	perror("Error al crear tuberia");
  }
  
  
  
  return 0;
  

}
